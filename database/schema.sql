create table nodejs.customer
(
	id int unsigned auto_increment
		primary key,
	name varchar(200) not null,
	address text not null,
	email varchar(50) not null,
	phone varchar(15) not null
);

insert into 'customer' ('id', 'name', 'address', 'email', 'phone') values('1','Richard Gere','Washington DC Street Block C 85','richardgere@yahoo.com','038372636232');
insert into 'customer' ('id', 'name', 'address', 'email', 'phone') values('2','Tukul Arwana','Jln Lingkaran subur 58','tukul@yahoo.com','038373273262');
insert into 'customer' ('id', 'name', 'address', 'email', 'phone') values('3','Taylor Swift','Hollywoord','tswift@yahoo.com','039223232323');
insert into 'customer' ('id', 'name', 'address', 'email', 'phone') values('4','Udin Gambut','Indonesia central of java','udingambut@gmail.com','0271 85858588');
insert into 'customer' ('id', 'name', 'address', 'email', 'phone') values('5','Uzumaki Naruto','Konohagakure','uzumaki_naruto@konohagakure.com','021 85858588');